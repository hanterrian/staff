<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Staff
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $post
 * @property string $hiring
 * @property float $amount
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereHiring($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff wherePost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Staff whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Staff extends Model
{
    use NodeTrait;
}
