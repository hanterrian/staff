<?php

use Illuminate\Database\Seeder;
use Cake\Chronos\Chronos;
use App\Staff;

class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->getOutput()->progressStart(1 * 5 * 10 * 20 * 50);

        $this->generateFirst();

        $this->command->getOutput()->progressFinish();
    }

    /**
     * @param int $level
     * @return array
     */
    protected function getStaffTemplate(int $level): array
    {
        $levels = [
            1 => [
                'label' => 'Level 1',
                'count' => 1,
                'amount' => [100000, 250000],
            ],
            2 => [
                'label' => 'Level 2',
                'count' => 5,
                'amount' => [80000, 100000],
            ],
            3 => [
                'label' => 'Level 3',
                'count' => 10,
                'amount' => [60000, 80000],
            ],
            4 => [
                'label' => 'Level 4',
                'count' => 20,
                'amount' => [50000, 60000],
            ],
            5 => [
                'label' => 'Level5 ',
                'count' => 50,
                'amount' => [30000, 50000],
            ],
        ];

        return $levels[$level];
    }

    /**
     * @return string
     */
    protected function firstName(): string
    {
        $list = [
            'Kendall',
            'Rowan',
            'Van',
            'Grace',
            'Sam',
            'Kamden',
            'Akira',
            'Armando',
            'Tommy',
            'Shayna',
            'Janiyah',
            'Valentina',
            'Kamren',
            'Keith',
            'Fabian',
            'Patricia',
            'Campbell',
            'Ali',
            'Natalie',
            'Emanuel',
            'Zayne',
            'Libby',
            'Sheldon',
            'Jensen',
            'Charity',
            'Simeon',
            'Theodore',
            'Anderson',
            'Jacey',
            'Gavyn',
            'Landen',
            'Zion',
            'Gwendolyn',
            'Elijah',
            'Rosemary',
            'Emmy',
        ];

        return array_random($list);
    }

    /**
     * @return mixed
     */
    protected function lastName(): string
    {
        $list = [
            'Thomas',
            'Rosario',
            'Burton',
            'Casey',
            'Kidd',
            'Graham',
            'Cameron',
            'Torres',
            'Richardson',
            'Mckee',
            'Hobbs',
            'Blackburn',
            'Watkins',
            'Duncan',
            'Douglas',
            'Henry',
            'Mccoy',
            'Harrison',
            'Webb',
            'Parks',
            'Cherry',
            'Everett',
            'Lewis',
            'Maldonado',
            'Irwin',
            'Ballard',
            'Mack',
            'Dougherty',
            'Deleon',
            'Parsons',
            'Ayala',
            'Haas',
            'Rivers',
            'Sellers',
            'Stevens',
            'Sexton',
        ];

        return array_random($list);
    }

    protected function generateFirst()
    {
        $items = $this->getStaffTemplate(1);

        $role = $items['label'];
        $count = $items['count'];
        $amount = rand($items['amount'][0], $items['amount'][1]);

        $chronos = new Chronos();

        for ($i = 0; $i < $count; $i++) {
            $staff = new Staff();

            $staff->image = 'http://via.placeholder.com/150x350';
            $staff->name = $this->firstName() . ' ' . $this->lastName();
            $staff->post = $role;
            $staff->hiring = $chronos->toDateString();
            $staff->amount = $amount;

            if ($staff->saveAsRoot()) {
                $this->command->getOutput()->progressAdvance();
                $this->generateSecond($staff);
            }
        }
    }

    protected function generateSecond(Staff $parent)
    {
        $items = $this->getStaffTemplate(2);

        $role = $items['label'];
        $count = $items['count'];
        $amount = rand($items['amount'][0], $items['amount'][1]);

        $chronos = new Chronos();

        for ($i = 0; $i < $count; $i++) {
            $chronos = $chronos->modify('+1 day');

            $staff = new Staff();

            $staff->image = 'http://via.placeholder.com/150x350';
            $staff->name = $this->firstName() . ' ' . $this->lastName();
            $staff->post = $role;
            $staff->hiring = $chronos->toDateString();
            $staff->amount = $amount;

            if ($staff->appendToNode($parent)->save()) {
                $this->command->getOutput()->progressAdvance();
                $this->generateThird($staff);
            }
        }
    }

    protected function generateThird(Staff $parent)
    {
        $items = $this->getStaffTemplate(3);

        $role = $items['label'];
        $count = $items['count'];
        $amount = rand($items['amount'][0], $items['amount'][1]);

        $chronos = new Chronos();

        for ($i = 0; $i < $count; $i++) {
            $chronos = $chronos->modify('+1 day');

            $staff = new Staff();

            $staff->image = 'http://via.placeholder.com/150x350';
            $staff->name = $this->firstName() . ' ' . $this->lastName();
            $staff->post = $role;
            $staff->hiring = $chronos->toDateString();
            $staff->amount = $amount;

            if ($staff->appendToNode($parent)->save()) {
                $this->command->getOutput()->progressAdvance();
                $this->generateFourth($staff);
            }
        }
    }

    protected function generateFourth(Staff $parent)
    {
        $items = $this->getStaffTemplate(4);

        $role = $items['label'];
        $count = $items['count'];
        $amount = rand($items['amount'][0], $items['amount'][1]);

        $chronos = new Chronos();

        for ($i = 0; $i < $count; $i++) {
            $chronos = $chronos->modify('+1 day');

            $staff = new Staff();

            $staff->image = 'http://via.placeholder.com/150x350';
            $staff->name = $this->firstName() . ' ' . $this->lastName();
            $staff->post = $role;
            $staff->hiring = $chronos->toDateString();
            $staff->amount = $amount;

            if ($staff->appendToNode($parent)->save()) {
                $this->command->getOutput()->progressAdvance();
                $this->generateFifth($staff);
            }
        }
    }

    protected function generateFifth(Staff $parent)
    {
        $items = $this->getStaffTemplate(5);

        $role = $items['label'];
        $count = $items['count'];
        $amount = rand($items['amount'][0], $items['amount'][1]);

        $chronos = new Chronos();

        for ($i = 0; $i < $count; $i++) {
            $chronos = $chronos->modify('+1 day');

            $staff = new Staff();

            $staff->image = 'http://via.placeholder.com/150x350';
            $staff->name = $this->firstName() . ' ' . $this->lastName();
            $staff->post = $role;
            $staff->hiring = $chronos->toDateString();
            $staff->amount = $amount;

            $staff->appendToNode($parent)->save();
            $this->command->getOutput()->progressAdvance();
        }
    }
}
